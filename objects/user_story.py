#!/usr/bin/env python
# coding: utf-8
from .exchanger import Exchanger
from utils import helpers


class UserStory:
    def __init__(self):
        self.exchanger = Exchanger()

    def start(self):
        while True:
            #  ______     ______   ______     ______     __  __
            # /\  ___\   /\__  _\ /\  __ \   /\  == \   /\ \_\ \
            # \ \___  \  \/_/\ \/ \ \ \/\ \  \ \  __<   \ \____ \
            #  \/\_____\    \ \_\  \ \_____\  \ \_\ \_\  \/\_____\
            #   \/_____/     \/_/   \/_____/   \/_/ /_/   \/_____/

            command_list = input("COMMAND?\n").strip().upper().split(' ')
            command = command_list[0]

            match command:
                case "COURSE":
                    # Validate command
                    if not helpers.is_valid_command(command_list):
                        continue
                    # Validate currency
                    currency = command_list[1]
                    if not helpers.is_valid_currency(currency):
                        continue
                    # Get Course
                    course = self.exchanger.get_current_course(currency)
                    if not helpers.is_valid_course(course):
                        continue
                    # Get limit
                    limit = self.exchanger.get_current_limit(currency)
                    print(f"RATE {course}, AVAILABLE {limit}")

                case "EXCHANGE":
                    # Validate command
                    if not helpers.is_valid_command(command_list, limit=2):
                        continue
                    # Validate currency
                    currency = command_list[1]
                    if not helpers.is_valid_currency(currency):
                        continue
                    # Validate currency value
                    currency_value = command_list[2]
                    if not helpers.is_float(currency_value):
                        continue
                    # Make exchange
                    self.exchanger.exchange(currency, float(currency_value))

                case "STOP":
                    # Stop service
                    print("SERVICE STOPPED")
                    break

                case _:
                    # Invalid command
                    print("INVALID COMMAND")
