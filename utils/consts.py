#!/usr/bin/env python
# coding: utf-8

CURRENCIES = {
    "base": "USD",
    "to": "UAH"
}

FILE_DATA = {
    "USD": 1000.0,
    "UAH": 10000.0
}
